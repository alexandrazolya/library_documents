# Documents Library

This repository contains a collection of general documents that are needed for both hardware and software development. All projects contain shortcuts to this repository.

As a rule, this repository should be placed on the same level with all the projects.